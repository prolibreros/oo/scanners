# Imágenes de _scanners_ de DIY Book Scanner

El nombre de la imagen hasta el guion bajo corresponde a la publicación
del foro.

Ejemplo: `f=14&t=1192_a.jpg` es `f=14&t=1192` de la url `https://forum.diybookscanner.org/viewtopic.php?f=14&t=1192`.
